package com.levelup.examples;

public enum SortMethod {

    BY_LASTNAME, BY_FIRSTNAME, BY_PHONE
}
